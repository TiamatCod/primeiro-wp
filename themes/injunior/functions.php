<?php 

  function inJunior_scripts(){
    wp_enqueue_style( 'reset', get_template_directory_uri() . '/assets/css/reset.css');
    wp_enqueue_style( 'style', get_template_directory_uri() . '/style.css');
  }

  add_action('wp_enqueue_scripts', 'inJunior_scripts');

  add_theme_support("title-tag");
  add_theme_support("menus");
?>